public class WordFrequency {
    private String word;
    private int frequency;

    public WordFrequency(String w, int i){
        this.word =w;
        this.frequency =i;
    }


    public String getWord() {
        return this.word;
    }

    public int getWordCount() {
        return this.frequency;
    }


    public String getWordCountString() {
        return this.word + " " + this.frequency;
    }
}
