import java.util.*;
import java.util.stream.Collectors;

public class WordFrequencyGame {

    public static final String REGEX = "\\s+";

    public String getResult(String inputStr){
        try {
            List<String> words = getWordListFromString(inputStr);
            Map<String, List<String>> groupedByWord = groupByWord(words);
            List<WordFrequency> wordFrequencyListWithCounts = getWordFrequencies(groupedByWord);
            return getWordFrequenciesString(wordFrequencyListWithCounts);
        } catch (Exception e) {
            return "Calculate Error";
        }
    }

    private static List<String> getWordListFromString(String inputStr) {
        return Arrays.stream(inputStr.split(REGEX))
                .collect(Collectors.toList());
    }


    private static String getWordFrequenciesString(List<WordFrequency> wordFrequencyListWithCounts) {
        wordFrequencyListWithCounts.sort((word1, word2) -> word2.getWordCount() - word1.getWordCount());
        return wordFrequencyListWithCounts.stream()
                .map(WordFrequency::getWordCountString)
                .collect(Collectors.joining("\n"));
    }

    private static List<WordFrequency> getWordFrequencies(Map<String, List<String>> map) {
        return map.entrySet().stream()
                .map(entry -> new WordFrequency(entry.getKey(), entry.getValue().size()))
                .collect(Collectors.toList());
    }

    private Map<String,List<String>> groupByWord(List<String> wordList) {
        return wordList.stream().collect(Collectors.groupingBy(word -> word));
    }


}
